### Co to? ###
Prosty skrypt do przypominania o robieniu przerw podczas pracy.

### Instalacja ###

* Zainstaluj node.js
* wejdź do folderu z repo
* odpal polecenie: 
```
#!bash

npm install
```
* następnie odpal:
```
#!bash

node break.js
```


### Konfiguracja ###

Ustawienia (treść wiadomości i interwał) można zmieniać w odpowiedniej sekcji w pliku break.js