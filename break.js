var notifier = require('node-notifier'),
    Jetty = require('jetty');

// settings:
var first = true,
    cycles = 0,
    interval = 90, // in minutes
    welcomeMessage = {
        title: 'Czesc',
        message: 'Milej pracy!'
    },
    breakMessage = {
        title: 'Przerwa',
        message: 'Odejdz od komputera na 5 minut!'
    };
// settings end

var jetty = new Jetty(process.stdout);
jetty.clear();
jetty.text("Minuty pozostałe do przerwy: ");

(function notifyMe() {
    var content;

    if (first === true) {
        notifier.notify(welcomeMessage);
        displayTime(interval);
        first = false;
    } else if (cycles === interval) {
        notifier.notify(breakMessage);
        displayTime(interval);
        cycles = 0;
    } else {
        ++cycles;
        displayTime(interval - cycles);
    }
    setTimeout(notifyMe, 1000 * 60);
})();

function displayTime(minutes) {
    jetty.moveTo([0, 29]);
    jetty.text('' + minutes);
    jetty.moveTo([1, 0]);
}
